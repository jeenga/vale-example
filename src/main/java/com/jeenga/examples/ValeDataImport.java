package com.jeenga.examples;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Scanner;

public class ValeDataImport {

    private static final String JEENGA_ENDPOINT = "https://api.jeenga.com/jeenga-api/customers/import/vale";

    private static String loadData(final String filename) throws FileNotFoundException {
        final ClassLoader classLoader = ValeDataImport.class.getClassLoader();
        final URL resource = classLoader.getResource(filename);
        final File file = new File(resource.getFile());
        final StringBuilder stringBuilder = new StringBuilder();

        try (final Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }
        }

        return stringBuilder.toString();
    }

    private static String sign(final String privateKey, final String dataToSign) throws Exception {
        final SecretKeySpec signingKey = new SecretKeySpec(privateKey.getBytes(), "HmacSHA1");
        final Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);
        final byte[] rawHmac = mac.doFinal(dataToSign.getBytes());
        return Base64.getEncoder().encodeToString(rawHmac);
    }

    private static String send(final String apiKey, final String data, final String signature) throws IOException {
        final HttpClient httpClient = buildHttpClient();
        final HttpPost httpPost = new HttpPost(JEENGA_ENDPOINT);

        httpPost.setEntity(new StringEntity(data, Charset.forName("UTF-8")));
        httpPost.addHeader(new BasicHeader("Content-Type", "application/json"));
        httpPost.addHeader(new BasicHeader("Authorization", apiKey + ":" + signature));

        final HttpResponse httpResponse = httpClient.execute(httpPost);
        final StatusLine statusLine = httpResponse.getStatusLine();

        if (statusLine.getStatusCode() == 200) {
            final InputStream content = httpResponse.getEntity().getContent();
            return IOUtils.toString(content, Charset.forName("UTF-8"));
        } else {
            throw new RuntimeException("Unable to execute. httpStatus = "
                    + statusLine.getStatusCode() + ", status = " + statusLine.getReasonPhrase());
        }
    }

    private static HttpClient buildHttpClient() {
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(buildRequestConfig())
                .build();
    }

    private static RequestConfig buildRequestConfig() {
        return RequestConfig.custom()
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(5000)
                .setSocketTimeout(5000)
                .build();
    }

    public static void main(String[] args) throws Exception {
        final String apiKey = "<YOUR_API_KEY_HERE>";
        final String privateKey = "<YOUR_PRIVATE_KEY_HERE>";

        final String data = loadData("ModeloVisitas.json");
        System.out.println("data = " + data);
        final String signature = sign(privateKey, data);
        System.out.println("signature = " + signature);
        final String response = send(apiKey, data, signature);
        System.out.println("response = " + response);
    }
}
